/*
 * Copyright (C) 2011-2014 CodingCrayons s.r.o.
 * All rights reserved.
 * Contact: CodingCrayons s.r.o. (info@codingcrayons.com)
 *
 * This file is part of AspectFaces.
 *
 * AspectFaces is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Foobar is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Foobar.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.codingcrayons.aspectfaces.ondemand;

import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.io.UnsupportedEncodingException;
import java.io.Writer;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

import javax.el.ELContext;
import javax.el.ExpressionFactory;
import javax.el.ValueExpression;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.view.facelets.ComponentConfig;
import javax.faces.view.facelets.FaceletContext;
import javax.faces.view.facelets.FaceletException;
import javax.faces.view.facelets.TagAttribute;

import com.codingcrayons.aspectfaces.AFWeaver;
import com.codingcrayons.aspectfaces.cache.ResourceCache;
import com.codingcrayons.aspectfaces.configuration.Context;
import com.codingcrayons.aspectfaces.exceptions.AFException;
import com.codingcrayons.aspectfaces.metamodel.JavaInspector;

import com.sun.faces.facelets.impl.GeneratorHandler;
import com.sun.faces.facelets.tag.TagAttributeImpl;
import javassist.util.proxy.ProxyObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Abstract Default AF assembler to Facelets
 */
public abstract class AFGeneratorHandler extends GeneratorHandler {

	private static final Logger LOGGER = LoggerFactory.getLogger(AFGeneratorHandler.class);
	private static final String DEFAULT_CONFIG = "default";
	private static final String ASPECT_FACES = "AspectFaces_ap";
	protected static Boolean IS_DEV_MODE = true;

	final private static String getStackTrace(Throwable aThrowable) {
		final Writer result = new StringWriter();
		final PrintWriter printWriter = new PrintWriter(result);
		aThrowable.printStackTrace(printWriter);
		return result.toString().replace("<", "&lt;").replace(">", "&gt;");
	}

	final protected static String viewFragmentExceptionString(Exception e) {
		StringBuilder sb = new StringBuilder(EMPTY_HEAD);
		sb.append("<div style='border:1px solid maroon;overflow:auto;padding:5px'>");
		sb.append("AspectFaces: ").append(e.getMessage());
		sb.append("</div>");
		sb.append("<div style='border:1px solid maroon;overflow:auto;padding:5px;font-size:10px;'>");
		sb.append(getStackTrace(e)).append("</div>").append(EMPTY_TAIL);

		return sb.toString();
	}

	final protected static ByteArrayInputStream viewFragmentExceptionIS(Exception e) {
		try {
			return new ByteArrayInputStream(viewFragmentExceptionString(e).getBytes("UTF-8"));
		} catch (UnsupportedEncodingException e1) {
			return new ByteArrayInputStream(viewFragmentExceptionString(e).getBytes());
		}
	}

	final protected static boolean isProxy(Object proxySuspect) {
		return proxySuspect instanceof ProxyObject;
	}

	protected final TagAttribute entityClassName;
	protected final TagAttribute configName;
	protected final TagAttribute instance;
	protected final TagAttribute layout;
	protected final TagAttribute ignore;
	protected Class<?>[] classesToInspect = null;
	protected AFWeaver afWeaver = null;
	protected FaceletContext ctx;

	private final String SEPARATOR = ",";
	private String alias = "unknown";

	public AFGeneratorHandler(ComponentConfig config) {
		this(config, -1);
	}

	public AFGeneratorHandler(ComponentConfig config, int afCacheTime) {
		super(config, afCacheTime);

		this.entityClassName = this.getAttribute("entity");
		this.configName = this.getAttribute("config");
		this.instance = this.getAttribute("instance");
		this.layout = this.getAttribute("layout");
		this.ignore = this.getAttribute("ignore");
		this.vars = this.tag.getAttributes().getAll();
		String dev = FacesContext.getCurrentInstance().getExternalContext().getInitParameter("javax.faces.PROJECT_STAGE");

		if ("Production".equalsIgnoreCase(dev)) {
			IS_DEV_MODE = false;
		}
		if (IS_DEV_MODE) {
			super.afCacheTime = 0;
		}
	}

	@Override
	protected void hookAboutToApply(FaceletContext ctx, UIComponent parent) {
		this.ctx = ctx;
		initiateClassesToInspect();
	}

	/**
	 * Gets AF per request
	 */
	protected void initializeAF() throws AFException {
		// per request!
		afWeaver = new AFWeaver(getConfig());
	}

	/**
	 * Gets AF from context if does not exist creates new
	 */
	@SuppressWarnings("unused")
	final private void initialize2() throws AFException {
		Lock mutex = new ReentrantLock();
		afWeaver = (AFWeaver) FacesContext.getCurrentInstance().getExternalContext().getSessionMap()
			.get(ASPECT_FACES);

		if (afWeaver == null) {
			mutex.lock();
			if (afWeaver == null) {
				// compiler = new NinjaCompiler();
				try {
					afWeaver = new AFWeaver(getConfig());
					FacesContext.getCurrentInstance().getExternalContext().getSessionMap()
						.put(ASPECT_FACES, afWeaver);
				} catch (Exception e) {
					throw new AFException("Cannot initiate AspectFaces instance", e);
				}
			}
			mutex.unlock();
		}
	}

	/**
	 * Initiates classes to inspect and inject varibale to context
	 */
	final protected void initiateClassesToInspect() {

		// to keep reference
		List<TagAttribute> variableList = new ArrayList<TagAttribute>();

		// to know which are set already
		Set<String> existingVariableNameSet = new HashSet<String>();

		variableList.addAll(Arrays.asList(this.tag.getAttributes().getAll()));
		for (TagAttribute ta : variableList) {
			existingVariableNameSet.add(ta.getNamespace() + ta.getLocalName() + ta.getLocation());
		}

		this.classesToInspect = initImplicitClassesToInspect();
		if (classesToInspect != null && classesToInspect.length != 0) {
			injectNewVariables(variableList, existingVariableNameSet, classesToInspect, getELInstances());
		} else {
			this.classesToInspect = initExplicitClassToInspect();
			if (classesToInspect != null && classesToInspect.length != 0) {
				injectNewVariables(variableList, existingVariableNameSet, classesToInspect,
					getELInstances());
			}
		}

		this.vars = variableList.toArray(new TagAttribute[variableList.size()]);
	}

	/**
	 * Initiates classes from instance
	 */
	final private Class<?>[] initImplicitClassesToInspect() {
		if (instance != null) {
			Object instanceObject;
			Class<?> inspectClass;

			String[] elInstances = getELInstances();

			List<Class<?>> inspectClasses = new ArrayList<Class<?>>();
			for (String instanceEL : elInstances) {
				// try to lookup what we work with
				instanceObject = evaluateExpressionInUIContext(ctx, instanceEL);

				if (instanceObject != null) {
					// instance identification
					if (isProxy(instanceObject)) {
						inspectClass = instanceObject.getClass().getSuperclass();
					} else {
						inspectClass = instanceObject.getClass();
					}
					inspectClasses.add(inspectClass);
				}
			}
			// return array, otherwise try success bellow
			if (!inspectClasses.isEmpty()) {
				return inspectClasses.toArray(new Class<?>[inspectClasses.size()]);
			}
		}

		return null;
	}

	/**
	 * Initiates class from entityClassName
	 */
	final private Class<?>[] initExplicitClassToInspect() {

		List<Class<?>> inspectClasses = new ArrayList<Class<?>>();
		if (entityClassName != null && entityClassName.getValue() != null) {
			String entity = (String) executeExpressionInElContext(ctx.getExpressionFactory(), ctx, entityClassName.getValue());
			if (entity == null) {
				return null;
			}
			// exact specification of what we work with - e.g. table row
			try {
				String[] instanceClassPaths = entity.split(SEPARATOR);
				for (String oneEntityClassName : instanceClassPaths) {
					inspectClasses.add(Class.forName(oneEntityClassName));
				}
			} catch (ClassNotFoundException e) {
				throw new FaceletException("Class: " + entityClassName.getValue() + " not found");
			}
			// return array
			return inspectClasses.toArray(new Class<?>[inspectClasses.size()]);
		}

		return null;
	}

	/**
	 * Injects a new variable name to the context
	 * <p/>
	 * if you pass class Person and PersonInfo through #{instance} it injects
	 * them as variables to the context
	 */
	final private void injectNewVariables(List<TagAttribute> variableList,
										  Set<String> existingVariableNameSet, Class<?>[] inspectClasses, String[] elInstances) {

		for (int i = 0; i < inspectClasses.length; ++i) {
			// prevent index out of bounds when class not found!
			addTemplateVariable(variableList, existingVariableNameSet, elInstances[i], inspectClasses[i]
				.getSimpleName());
		}
	}

	final private String[] getELInstances() {
		// developer does not specify the type -> lookup in JSF or Facelet context
		String instanceELUnparsed = instance.getValue();

		// it is allowed to have multiple classes at once
		String[] instanceELArray = instanceELUnparsed.split(SEPARATOR);
		return instanceELArray;
	}

	/**
	 * Adds template variable if not exists already or replace
	 */
	final private void addTemplateVariable(List<TagAttribute> variableList, Set<String> existingVariableNameSet,
										   String el, String localName) {

		String varKey = instance.getNamespace() + localName + instance.getLocation();

		TagAttribute newVariable = new TagAttributeImpl(instance.getLocation(), instance.getNamespace(), localName,
			localName, el);

		if (!existingVariableNameSet.contains(varKey)) {
			// add if not exits
			variableList.add(newVariable);
			existingVariableNameSet.add(varKey);
		} else {
			// replace the list
			//List<TagAttribute> newList = new ArrayList<TagAttribute>();
			Integer index = null;
			for (int i = 0; i < variableList.size(); ++i) {
				TagAttribute attr = variableList.get(i);

				if ((attr.getNamespace() + attr.getLocalName() + attr.getLocation()).equals(varKey)) {
					index = i;
					break;
				}
			}
			if (index != null) {
				variableList.remove(index);
				variableList.add(index, newVariable);
			}
		}
	}

	/**
	 * Evaluates EL in AF and in Facelets!
	 */
	final protected static Object evaluateExpressionInUIContext(final FaceletContext facCtx, final String expression) {
		// context from facelets
		Object result = executeExpressionInElContext(facCtx.getExpressionFactory(), facCtx, expression);
		if (null == result) {
			// if not found then lookup faces context
			final ELContext elContext = facCtx.getFacesContext().getELContext();
			result = executeExpressionInElContext(facCtx.getExpressionFactory(), elContext, expression);
		}

		return result;
	}

	final protected static Object executeExpressionInElContext(ExpressionFactory expressionFactory,
															   ELContext elContext, String expression) {
		ValueExpression exp = expressionFactory.createValueExpression(elContext, expression, Object.class);
		return exp == null ? null : exp.getValue(elContext);
	}

	/**
	 * Generates ui and returns IS
	 */
	final protected InputStream generateAsIS(Class<?>[] clazz, String configName, String[] roles,
											 List<String> profiles, String layout) {
		return createInputStream(generateAsString(clazz, configName, roles, profiles, layout));
	}

	/**
	 * Generates ui and returns IS
	 */
	final protected String generateAsString(Class<?>[] clazz, String configName, String[] roles,
											List<String> profiles, String layout) {

		if (clazz == null || clazz.length == 0) {
			return EMPTY_HEAD + " nothing to inspect " + EMPTY_TAIL;
		}

		try {
			alias = getFragmentName(clazz);
			Context context = new Context(alias, profiles.toArray(new String[profiles.size()]), roles, layout, true);

			if (ignore != null) {
				// Ignored fields
				String expressedIgnoreString = (String) executeExpressionInElContext(ctx.getExpressionFactory(), ctx, ignore.getValue());
				if (expressedIgnoreString != null && !expressedIgnoreString.trim().isEmpty()) {
					context.getContextIgnoreSet().addAll(Arrays.asList(expressedIgnoreString.split(SEPARATOR)));
				}
			}
			// Possible extension
			hookAddToAFContext(context);

			afWeaver.setConfiguration(configName);
			afWeaver.setInspector(new JavaInspector(clazz));

			return afWeaver.generate(context);
		} catch (AFException e) {
			return viewFragmentExceptionString(e);
		}
	}

	protected void hookAddToAFContext(Context context) {
		// context.getVariables().put("tom", "Tomas");
	}

	/**
	 * String to IS convertor allow to inherit and add syso
	 */
	protected InputStream createInputStream(String s) {
		try {
			if (LOGGER.isTraceEnabled()) {
				LOGGER.trace(s);
			}
			if (IS_DEV_MODE) {
				ResourceCache.getInstance().clear();
			}
			return new ByteArrayInputStream(s.getBytes("UTF-8"));
		} catch (UnsupportedEncodingException e) {
			return viewFragmentExceptionIS(e);
		}
	}

	/**
	 * Generates fragment name
	 */
	final protected static String getFragmentName(Class<?>[] clazz) {
		StringBuilder out = new StringBuilder();
		if (clazz != null) {
			for (Class<?> class1 : clazz) {
				out.append(class1.getSimpleName());
			}
		}
		return out.toString();
	}

	@Override
	protected String getAlias() {
		return alias;
	}

	protected String getConfig() {
		if (configName == null || configName.getValue().isEmpty()) {
			return DEFAULT_CONFIG;
		}
		return configName.getValue();
	}
}
