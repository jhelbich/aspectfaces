/*
 * Copyright (C) 2011-2014 CodingCrayons s.r.o.
 * All rights reserved.
 * Contact: CodingCrayons s.r.o. (info@codingcrayons.com)
 *
 * This file is part of AspectFaces.
 *
 * AspectFaces is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Foobar is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Foobar.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.sun.faces.facelets.impl;

import java.io.BufferedInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

import javax.el.ELException;
import javax.el.VariableMapper;
import javax.faces.FacesException;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.view.facelets.ComponentConfig;
import javax.faces.view.facelets.FaceletCache;
import javax.faces.view.facelets.FaceletContext;
import javax.faces.view.facelets.FaceletException;
import javax.faces.view.facelets.TagAttribute;

import com.sun.faces.facelets.Facelet;
import com.sun.faces.facelets.FaceletContextImplBase;
import com.sun.faces.facelets.FaceletFactory;
import com.sun.faces.facelets.LiteCache;
import com.sun.faces.facelets.TemplateClient;
import com.sun.faces.facelets.compiler.Compiler;
import com.sun.faces.facelets.compiler.LiteCompiler;
import com.sun.faces.facelets.compiler.SAXCompiler;
import com.sun.faces.facelets.el.VariableMapperWrapper;
import com.sun.faces.facelets.tag.TagHandlerImpl;
import com.sun.faces.facelets.tag.ui.DefineHandler;
import com.sun.faces.facelets.tag.ui.ParamHandler;
import com.sun.faces.facelets.util.Resource;
import com.sun.faces.util.Cache;
import com.sun.faces.util.RequestStateManager;

/**
 * Handler allowing to add InputStream to facelet Decorate component
 * Usage: extend and apply hookMethods with your own content
 */
public class GeneratorHandler extends TagHandlerImpl implements TemplateClient {

	private static final String ASPECT_FACES_COMPILER = "AspectFacesCompiler";
	private static final String ASPECT_FACES_CACHE = "AspectFacesCache";
	private static final String ASPECT_FACES_ID_MAPPERS = "AspectFacesIdMappers";

	// in case no instance is provided we render empty fragment
	protected static final String EMPTY_HEAD;
	protected static final String EMPTY_TAIL;

	static {
		StringBuilder sb = new StringBuilder();
		sb.append("<!DOCTYPE html PUBLIC \"-//W3C//DTD XHTML 1.0 Strict//EN\" ");
		sb.append("\"http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd\">");
		sb.append("<html xmlns=\"http://www.w3.org/1999/xhtml\">");
		EMPTY_HEAD = sb.toString();
		EMPTY_TAIL = "</html>";
	}

	protected com.sun.faces.facelets.compiler.Compiler originalCompiler = null;
	protected FaceletCache<DefaultFacelet> originalCache = null;
	protected Cache<String, IdMapper> originalIdMappers = null;
	protected final Map<String, Object> handlers;
	protected ParamHandler[] params;
	/**
	 * caching time, -1 = forever
	 */
	protected int afCacheTime = -1;
	protected TagAttribute[] vars;

	private final TagAttribute value;
	protected final TagAttribute id;
	protected final String rendererType;
	protected final TagAttribute binding;
	protected final String componentType;

	public GeneratorHandler(ComponentConfig config, int afCacheTime) {
		this(config);
		this.afCacheTime = afCacheTime;
	}

	public GeneratorHandler(ComponentConfig config) {
		super(config);

		this.componentType = config.getComponentType();
		this.rendererType = config.getRendererType();
		this.value = this.getAttribute("value");
		this.id = this.getAttribute("id");
		this.binding = this.getAttribute("binding");
		this.handlers = new HashMap<String, Object>();

		Iterator<?> itr = this.findNextByType(DefineHandler.class);
		while (itr.hasNext()) {
			DefineHandler d = (DefineHandler) itr.next();
			this.handlers.put(d.getName(), d);
		}
		List<Object> paramC = new ArrayList<Object>();
		itr = this.findNextByType(ParamHandler.class);
		while (itr.hasNext()) {
			paramC.add(itr.next());
		}
		if (paramC.size() > 0) {
			this.params = new ParamHandler[paramC.size()];
			for (int i = 0; i < this.params.length; i++) {
				this.params[i] = (ParamHandler) paramC.get(i);
			}
		} else {
			this.params = null;
		}
		this.vars = this.tag.getAttributes().getAll();
	}

	private URL resolveUrl(String path) {
		try {
			return Resource.getResourceUrl(FacesContext.getCurrentInstance(), path);
		} catch (IOException e) {
			throw new FacesException(e);
		}
	}

	/**
	 * @param faceletCtx
	 * @param parent
	 * @throws IOException
	 * @throws FacesException
	 * @throws ELException
	 * @see com.sun.facelets.FaceletHandler#apply(com.sun.facelets.FaceletContext, javax.faces.component.UIComponent)
	 */
	@Override
	public void apply(FaceletContext faceletCtx, UIComponent parent) throws IOException, FacesException, ELException {
		hookAboutToApply(faceletCtx, parent);

		FaceletContextImplBase ctx = (FaceletContextImplBase) faceletCtx;
		VariableMapper orig = ctx.getVariableMapper();
		if (this.params != null || this.vars.length > 0) {
			VariableMapper varMapper = new VariableMapperWrapper(orig);
			ctx.setVariableMapper(varMapper);
			if (this.params != null) {
				for (ParamHandler param : this.params) {
					param.apply(ctx, parent);
				}
			}
			// setup a variable map
			if (this.vars.length > 0) {
				for (TagAttribute var : this.vars) {
					varMapper.setVariable(var.getLocalName(),
						var.getValueExpression(ctx, Object.class));
				}
			}
		}

		ctx.pushClient(this);
		try {
			// hook
			InputStream is = hookInputStreamToApply();

			initialize();

			// this unfortunately does not work
			// DefaultFaceletCache cache = new DefaultFaceletCache(afCacheTime);
			LiteFaceletFactory factory = new LiteFaceletFactory(originalCompiler, new DefaultResourceResolver(), afCacheTime, originalCache);
			// force the originalIdMappers
			setIdMappers(factory, originalIdMappers);

			Facelet facelet = factory.getFacelet(is, getAlias());
			// this unfortunately does not work
			Method m = getIncludeMethod(facelet);
			m.invoke(facelet, ctx, parent);

			hookApplyOver(faceletCtx, parent);
		} catch (Exception e) {
			throw new FaceletException(e);
		} finally {
			ctx.setVariableMapper(orig);
			ctx.popClient(this);
		}
	}

	@Override
	public boolean apply(FaceletContext ctx, UIComponent parent, String name) throws IOException, FacesException, ELException {
		if (name != null) {
			DefineHandler handler = (DefineHandler) this.handlers.get(name);
			if (handler != null) {
				handler.applyDefinition(ctx, parent);
				return true;
			} else {
				return false;
			}
		} else {
			this.nextHandler.apply(ctx, parent);
			return true;
		}
	}

	protected void hookAboutToApply(FaceletContext ctx, UIComponent parent) {
	}

	protected String getAlias() {
		return "unnamed";
	}

	protected void hookApplyOver(FaceletContext ctx, UIComponent parent) {
	}

	protected InputStream hookInputStreamToApply() {
		try {
			String loadTemplate = this.value.getValue();
			URL url = resolveUrl(loadTemplate);
			return new BufferedInputStream(url.openStream(), 1024);
		} catch (IOException e) {
			throw new FacesException(e);
		}
	}

	/**
	 * Init GlueCompiler (that extends Facelet functionality) and places it to Application context
	 */
	@SuppressWarnings("unchecked")
	final protected void initialize() {
		Lock mutex = new ReentrantLock();
		Map<String, Object> app = FacesContext.getCurrentInstance().getExternalContext().getApplicationMap();

		originalCompiler = (Compiler) app.get(ASPECT_FACES_COMPILER);

		if (originalCompiler == null) {
			mutex.lock();
			if (originalCompiler == null) {
				SAXCompiler rootCompiler = getDefinedCompiler();
				originalCompiler = new LiteCompiler(rootCompiler);
				app.put(ASPECT_FACES_COMPILER, originalCompiler);
			}
			mutex.unlock();
		}

		originalCache = (FaceletCache<DefaultFacelet>) app.get(ASPECT_FACES_CACHE);

		if (originalCache == null) {
			mutex.lock();
			if (originalCache == null) {
				originalCache = getDefinedCache();
				FacesContext.getCurrentInstance().getExternalContext().getApplicationMap()
					.put(ASPECT_FACES_CACHE, originalCache);
			}
			mutex.unlock();
		}
		originalIdMappers = (Cache<String, IdMapper>) app.get(ASPECT_FACES_ID_MAPPERS);

		if (originalIdMappers == null) {
			mutex.lock();
			if (originalIdMappers == null) {
				originalIdMappers = getDefinedIdMappers();
				app.put(ASPECT_FACES_ID_MAPPERS, originalIdMappers);
			}
			mutex.unlock();
		}
	}

	@SuppressWarnings("unchecked")
	private FaceletCache<DefaultFacelet> getDefinedCache() {
		try {
			FacesContext context = FacesContext.getCurrentInstance();
			FaceletFactory faceletFactory = (FaceletFactory) RequestStateManager.get(context, RequestStateManager.FACELET_FACTORY);
			return (FaceletCache<DefaultFacelet>) getField(faceletFactory, "cache");
		} catch (Exception e) {
			throw new FacesException(e);
		}
	}

	@SuppressWarnings("unchecked")
	private Cache<String, IdMapper> getDefinedIdMappers() {
		try {
			// This plays role when assigning IDs
			FacesContext context = FacesContext.getCurrentInstance();
			FaceletFactory faceletFactory = (FaceletFactory) RequestStateManager.get(context, RequestStateManager.FACELET_FACTORY);
			return (Cache<String, IdMapper>) getField(faceletFactory, "idMappers");
		} catch (Exception e) {
			throw new FacesException(e);
		}
	}

	/**
	 * Gets existing initialized Compiler from FaceletFactory
	 */
	private SAXCompiler getDefinedCompiler() {
		try {
			FacesContext context = FacesContext.getCurrentInstance();
			FaceletFactory faceletFactory = (FaceletFactory) RequestStateManager.get(context, RequestStateManager.FACELET_FACTORY);
			return (SAXCompiler) getField(faceletFactory, "compiler");
		} catch (Exception e) {
			throw new FacesException(e);
		}
	}

	/**
	 * Gets private field from FaceletFactory
	 */
	private Object getField(FaceletFactory f, String fieldName) {
		try {
			Field field = LiteCache.getField("GeneratorHandler#" + fieldName);
			if (field == null) {
				field = f.getClass().getDeclaredField(fieldName);
				field.setAccessible(true);

				LiteCache.putField("GeneratorHandler#" + fieldName, field);
			}
			return field.get(f);
		} catch (Exception e) {
			throw new FacesException(e);
		}
	}

	private static Method getIncludeMethod(Facelet facelet) throws NoSuchMethodException, SecurityException {
		try {
			Method method = LiteCache.getMethod("DefaultFacelet#include");
			if (method == null) {

				method = facelet.getClass().getDeclaredMethod("include", Class.forName("com.sun.faces.facelets.impl.DefaultFaceletContext"), UIComponent.class);
				method.setAccessible(true);

				LiteCache.putMethod("DefaultFacelet#include", method);
			}

			return method;
		} catch (ClassNotFoundException e) {
			throw new FacesException(e);
		}
	}

	private static void setIdMappers(LiteFaceletFactory factory, Cache<String, IdMapper> idMappers) {
		try {
			Field field = LiteCache.getField("LiteFaceletFactory#idMappers");

			if (field == null) {
				field = LiteFaceletFactory.class.getSuperclass().getDeclaredField("idMappers");
				field.setAccessible(true);

				LiteCache.putField("LiteFaceletFactory#idMappers", field);
			}
			field.set(factory, idMappers);
		} catch (NoSuchFieldException e) {
			throw new FacesException(e);
		} catch (IllegalArgumentException e) {
			throw new FacesException(e);
		} catch (IllegalAccessException e) {
			throw new FacesException(e);
		}
	}
}
