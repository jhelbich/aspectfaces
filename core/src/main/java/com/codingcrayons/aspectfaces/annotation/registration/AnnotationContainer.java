/*
 * Copyright (C) 2011-2014 CodingCrayons s.r.o.
 * All rights reserved.
 * Contact: CodingCrayons s.r.o. (info@codingcrayons.com)
 *
 * This file is part of AspectFaces.
 *
 * AspectFaces is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Foobar is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Foobar.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.codingcrayons.aspectfaces.annotation.registration;

import java.util.HashMap;
import java.util.concurrent.locks.ReentrantLock;

import com.codingcrayons.aspectfaces.annotation.registration.pointCut.EvaluableJoinPoint;
import com.codingcrayons.aspectfaces.annotation.registration.pointCut.OrderJoinPoint;
import com.codingcrayons.aspectfaces.annotation.registration.pointCut.SecurityJoinPoint;
import com.codingcrayons.aspectfaces.annotation.registration.pointCut.VariableJoinPoint;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class AnnotationContainer {

	private static final Logger LOGGER = LoggerFactory.getLogger(AnnotationContainer.class);
	private static AnnotationContainer instance;
	private static final ReentrantLock lock = new ReentrantLock();

	private final HashMap<String, EvaluableJoinPoint> evaluableAnnotations;
	private final HashMap<String, VariableJoinPoint> joinPointVariableAnnotations;
	private final HashMap<String, OrderJoinPoint> orderableAnnotations;
	private final HashMap<String, SecurityJoinPoint> roleableAnnotations;

	/**
	 * Drop current instance (if exists) and create new AnnotationContainer.
	 */
	public static void restart() {
		instance = new AnnotationContainer();
	}

	/**
	 * @return AnnotationContainer singleton instance
	 */
	public static AnnotationContainer getInstance() {
		if (instance == null) {
			lock.lock();
			try {
				if (instance == null) {
					instance = new AnnotationContainer();
				}
			} finally {
				lock.unlock();
			}
		}
		return instance;
	}

	private AnnotationContainer() {
		this.evaluableAnnotations = new HashMap<String, EvaluableJoinPoint>();
		this.joinPointVariableAnnotations = new HashMap<String, VariableJoinPoint>();
		this.orderableAnnotations = new HashMap<String, OrderJoinPoint>();
		this.roleableAnnotations = new HashMap<String, SecurityJoinPoint>();
	}

	public void registerAnnotation(AnnotationDescriptor annotationDescriptor) {

		if (annotationDescriptor instanceof EvaluableJoinPoint) {
			this.evaluableAnnotations.put(annotationDescriptor.getAnnotationName(), (EvaluableJoinPoint) annotationDescriptor);
		}
		if (annotationDescriptor instanceof VariableJoinPoint) {
			this.joinPointVariableAnnotations.put(annotationDescriptor.getAnnotationName(),
				(VariableJoinPoint) annotationDescriptor);
		}
		if (annotationDescriptor instanceof OrderJoinPoint) {
			this.orderableAnnotations.put(annotationDescriptor.getAnnotationName(), (OrderJoinPoint) annotationDescriptor);
		}
		if (annotationDescriptor instanceof SecurityJoinPoint) {
			this.roleableAnnotations.put(annotationDescriptor.getAnnotationName(), (SecurityJoinPoint) annotationDescriptor);
		}

		LOGGER.trace("Annotation descriptor for: '{}' annotation registered", annotationDescriptor.getAnnotationName());
	}

	public EvaluableJoinPoint getEvaluableAnnotation(String name) {
		return this.evaluableAnnotations.get(name);
	}

	public VariableJoinPoint getVariableJoinPointAnnotation(String name) {
		return this.joinPointVariableAnnotations.get(name);
	}

	public OrderJoinPoint getOrderableAnnotation(String name) {
		return this.orderableAnnotations.get(name);
	}

	public SecurityJoinPoint getRoleableAnnotation(String name) {
		return this.roleableAnnotations.get(name);
	}
}
