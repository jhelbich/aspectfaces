/*
 * Copyright (C) 2011-2014 CodingCrayons s.r.o.
 * All rights reserved.
 * Contact: CodingCrayons s.r.o. (info@codingcrayons.com)
 *
 * This file is part of AspectFaces.
 *
 * AspectFaces is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Foobar is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Foobar.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.codingcrayons.aspectfaces.cache;

import java.util.Map;
import java.util.TreeMap;
import java.util.concurrent.locks.ReentrantLock;

public class SimpleCache implements CacheProvider {

	private static final String DEFAULT_REGION = "/";
	private static final ReentrantLock lock = new ReentrantLock();

	private Map<String, Map<String, String>> regions;
	private String defaultRegion;

	/**
	 * Constructs a new cache with one default region.
	 */
	public SimpleCache() {
		defaultRegion = SimpleCache.DEFAULT_REGION;
		initCache();
	}

	/**
	 * Creates a new map for cache regions and puts there the default region.
	 */
	private void initCache() {
		regions = new TreeMap<String, Map<String, String>>();
		regions.put(defaultRegion, new TreeMap<String, String>());
	}

	/**
	 * Returns the value to which the specified key is mapped in the default region, or null if this map contains no
	 * mapping for the key.
	 *
	 * @param key by which is a cache item saved in the region
	 * @return the value to which the specified key is mapped, or null if the default cache region contains no mapping
	 * for the key
	 */
	@Override
	public String get(String key) {
		return get(defaultRegion, key);
	}

	/**
	 * Returns the value to which the specified key is mapped in the specified region, or null if this map contains no
	 * mapping for the key.
	 *
	 * @param region in which is a cache item saved
	 * @param key    by which is a cache item saved in the region
	 * @return the value to which the specified key is mapped, or null if the specified cache region contains no mapping
	 * for the key
	 */
	@Override
	public String get(String region, String key) {
		Map<String, String> cacheRegion = regions.get(region);
		return cacheRegion == null ? null : cacheRegion.get(key);
	}

	/**
	 * Returns a name of the default region.
	 *
	 * @return name of the default region
	 */
	@Override
	public String getDefaultRegion() {
		return defaultRegion;
	}

	/**
	 * Sets a new name of the default region. If new name == null, then start value is being set.
	 *
	 * @param defaultRegion new name for default region.
	 */
	@Override
	public void setDefaultRegion(String defaultRegion) {
		lock.lock();
		try {
			if (defaultRegion == null) {
				this.defaultRegion = SimpleCache.DEFAULT_REGION;
			} else {
				this.defaultRegion = defaultRegion;
			}
		} finally {
			lock.unlock();
		}
	}

	/**
	 * Puts a value to the default cache region under the specified key. If the cache previously contained a mapping
	 * for the key, the old value is replaced by the specified value.
	 *
	 * @param key   to identify a value
	 * @param value to put
	 */
	@Override
	public void put(String key, String value) {
		put(defaultRegion, key, value);
	}

	/**
	 * Puts a value to the specified cache region under the specified key. If the cache previously contained a mapping
	 * for the key, the old value is replaced by the specified value.
	 *
	 * @param region to put a value
	 * @param key    to identify a value
	 * @param value  to put
	 */
	@Override
	public void put(String region, String key, String value) {
		Map<String, String> cacheRegion = findOrCreateRegion(region);
		cacheRegion.put(key, value);
	}

	/**
	 * Finds the cache region, or creates it if it does not already exist.
	 *
	 * @param region to find or create
	 * @return region
	 */
	private Map<String, String> findOrCreateRegion(String region) {
		Map<String, String> cacheRegion = regions.get(region);
		if (cacheRegion == null) {
			lock.lock();
			try {
				cacheRegion = regions.get(region);
				if (cacheRegion == null) {
					cacheRegion = new TreeMap<String, String>();
					regions.put(region, cacheRegion);
				}
			} finally {
				lock.unlock();
			}
		}
		return cacheRegion;
	}

	/**
	 * Clear all regions. The default region is not being restored to the start value.
	 */
	public void clear() {
		lock.lock();
		try {
			initCache();
		} finally {
			lock.unlock();
		}
	}

	/**
	 * Removes the entire region. If region == null, the default region is being cleared
	 *
	 * @param region to clear
	 */
	public void clear(String region) {
		lock.lock();
		try {
			if (region != null) {
				regions.remove(region);
			} else {
				regions.remove(defaultRegion);
			}
		} finally {
			lock.unlock();
		}
	}

	/**
	 * Removes all regions and restores the default region to the start value.
	 */
	public void reset() {
		lock.lock();
		try {
			defaultRegion = SimpleCache.DEFAULT_REGION;
			initCache();
		} finally {
			lock.unlock();
		}
	}

	/**
	 * Removes the value mapped to the specified key from the default region.
	 *
	 * @param key by which is a cache item saved in the default region
	 */
	public void remove(String key) {
		remove(defaultRegion, key);
	}

	/**
	 * Removes the value mapped to the specified key from the specified region.
	 *
	 * @param region from which a cache item is being removed
	 * @param key    by which is a cache item saved in the region
	 */
	public void remove(String region, String key) {
		lock.lock();
		try {
			Map<String, String> cacheRegion = regions.get(region);
			if (cacheRegion != null) {
				cacheRegion.remove(key);
			}
		} finally {
			lock.unlock();
		}
	}
}
