/*
 * Copyright (C) 2011-2014 CodingCrayons s.r.o.
 * All rights reserved.
 * Contact: CodingCrayons s.r.o. (info@codingcrayons.com)
 *
 * This file is part of AspectFaces.
 *
 * AspectFaces is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Foobar is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Foobar.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.codingcrayons.aspectfaces.configuration;

import java.io.File;

import com.codingcrayons.aspectfaces.cache.ResourceCache;
import com.codingcrayons.aspectfaces.exceptions.TemplateFileAccessException;
import com.codingcrayons.aspectfaces.exceptions.TemplateFileNotFoundException;
import com.codingcrayons.aspectfaces.util.Files;
import com.codingcrayons.aspectfaces.util.Strings;

public class StaticConfiguration extends Configuration {

	public StaticConfiguration(String name) {
		super(name);
	}

	@Override
	public void setSettings(Settings settings) {
		if (settings.getRealPath() == null) {
			settings.setRealPath(System.getProperty("user.dir"));
		}
		super.setSettings(settings);
	}

	@Override
	protected StringBuilder getTemplate(String fileTemplatePath, String prefix) throws TemplateFileNotFoundException {
		if (Strings.isNotBlank(fileTemplatePath)) {
			// do not use File.separator for win
			String filePath = this.getSettings().getRealPath() + prefix + getDelimiter() + fileTemplatePath;

			String cacheKey = this.name + filePath;

			if (!ResourceCache.getInstance().containsTemplate(cacheKey)) {
				// not cached
				try {
					String tagContent = Files.readString(filePath);
					// add to cache
					StringBuilder tagContentStringBuilder = new StringBuilder(tagContent);

					ResourceCache.getInstance().putTemplate(cacheKey, tagContentStringBuilder);
					// keep the method here in case we disable cache
					return tagContentStringBuilder;
				} catch (TemplateFileAccessException e) {
					throw new TemplateFileNotFoundException("File " + filePath + " not found.", e);
				}
			}
			return ResourceCache.getInstance().getTemplate(cacheKey);
		} else {
			return null;
		}
	}

	@Override
	public String getAbsolutePath() {
		return this.getSettings().getRealPath();
	}

	@Override
	protected String getDelimiter() {
		return File.separator;
	}
}
