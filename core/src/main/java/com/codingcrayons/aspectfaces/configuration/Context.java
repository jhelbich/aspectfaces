/*
 * Copyright (C) 2011-2014 CodingCrayons s.r.o.
 * All rights reserved.
 * Contact: CodingCrayons s.r.o. (info@codingcrayons.com)
 *
 * This file is part of AspectFaces.
 *
 * AspectFaces is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Foobar is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Foobar.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.codingcrayons.aspectfaces.configuration;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import com.codingcrayons.aspectfaces.annotation.registration.pointCut.properties.Variable;

public class Context {

	private static final String DEFAULT_ORDER_ANNOTATION = "com.codingcrayons.aspectfaces.annotations.UiOrder";
	private static final String DEFAULT_PROFILES_ANNOTATION = "com.codingcrayons.aspectfaces.annotations.UiProfiles";
	private static final String DEFAULT_USER_ROLES_ANNOTATION = "com.codingcrayons.aspectfaces.annotations.UiUserRoles";

	private static String defaultOrderAnnotation = DEFAULT_ORDER_ANNOTATION;
	private static String defaultProfilesAnnotation = DEFAULT_PROFILES_ANNOTATION;
	private static String defaultUserRolesAnnotation = DEFAULT_USER_ROLES_ANNOTATION;

	public static void setDefaultOrderAnnotation(String defaultOrderAnnotationName) {
		defaultOrderAnnotation = defaultOrderAnnotationName;
	}

	public static void setDefaultProfilesAnnotation(String defaultProfilesAnnotationName) {
		Context.defaultProfilesAnnotation = defaultProfilesAnnotationName;
	}

	public static void setDefaultUserRolesAnnotation(String defaultUserRolesAnnotationName) {
		defaultUserRolesAnnotation = defaultUserRolesAnnotationName;
	}

	public static void reset() {
		defaultOrderAnnotation = DEFAULT_ORDER_ANNOTATION;
		defaultProfilesAnnotation = DEFAULT_PROFILES_ANNOTATION;
		defaultUserRolesAnnotation = DEFAULT_USER_ROLES_ANNOTATION;
	}

	private String fragmentName;
	private String[] profiles;
	private String[] roles;
	private final Set<String> ignoreSet = new HashSet<String>();
	private String layout;
	private boolean useCover;
	private boolean collate = false;
	private String orderAnnotation;
	private String profilesAnnotation;
	private String userRolesAnnotation;
	private Configuration configuration;
	private Map<String, Object> variables = new HashMap<String, Object>();
	private List<Variable> variableList;

	public Context() {
		this(null, null, null, null, false);
	}

	public Context(String fragmentName) {
		this(fragmentName, null, null, null, false);
	}

	public Context(String fragmentName, String[] profiles, String[] roles, String layout, boolean useCover) {
		this(fragmentName, profiles, roles, layout, useCover, defaultOrderAnnotation, defaultProfilesAnnotation,
			defaultUserRolesAnnotation);
	}

	public Context(String fragmentName, String[] profiles, String[] roles, String layout, boolean useCover,
				   String orderAnnotation, String profilesAnnotation, String userRolesAnnotation) {
		this.fragmentName = fragmentName;
		this.profiles = profiles;
		this.roles = roles;
		this.layout = layout;
		this.useCover = useCover;
		this.orderAnnotation = orderAnnotation;
		this.profilesAnnotation = profilesAnnotation;
		this.userRolesAnnotation = userRolesAnnotation;
		this.configuration = null;
	}

	public String getOrderAnnotation() {
		return this.orderAnnotation;
	}

	public void setOrderAnnotation(String orderAnnotation) {
		this.orderAnnotation = orderAnnotation;
	}

	public String getProfilesAnnotation() {
		return this.profilesAnnotation;
	}

	public void setProfilesAnnotation(String profilesAnnotation) {
		this.profilesAnnotation = profilesAnnotation;
	}

	public String getUserRolesAnnotation() {
		return this.userRolesAnnotation;
	}

	public void setUserRolesAnnotation(String userRolesAnnotation) {
		this.userRolesAnnotation = userRolesAnnotation;
	}

	public Configuration getConfiguration() {
		return configuration;
	}

	public void setConfiguration(Configuration configuration) {
		this.configuration = configuration;
	}

	public String getFragmentName() {
		return fragmentName;
	}

	public void setFragmentName(String fragmentName) {
		this.fragmentName = fragmentName;
	}

	public String getLayout() {
		return layout;
	}

	public void setLayout(String layout) {
		this.layout = layout;
	}

	public String[] getProfiles() {
		return profiles;
	}

	public void setProfiles(String[] profiles) {
		this.profiles = profiles;
	}

	public String[] getRoles() {
		return roles;
	}

	public void setRoles(String[] roles) {
		this.roles = roles;
	}

	public boolean isUseCover() {
		return useCover;
	}

	public void setUseCover(boolean useCover) {
		this.useCover = useCover;
	}

	public Map<String, Object> getVariables() {
		return variables;
	}

	public void setVariables(Map<String, Object> variables) {
		this.variables = variables;
		variableList = null;
	}

	public List<Variable> getVariableList() {
		if (variableList == null) {
			variableList = new ArrayList<Variable>(variables.size());
			for (Entry<String, Object> entry : variables.entrySet()) {
				variableList.add(new Variable(entry.getKey(), entry.getValue()));
			}
		}
		return variableList;
	}

	public boolean isContextIgnored(String field) {
		return ignoreSet.contains(field);
	}

	public Set<String> getContextIgnoreSet() {
		return ignoreSet;
	}

	/**
	 * When composition consists of 2 or more classes should we order globally or per class
	 *
	 * @return
	 */
	public boolean isCollate() {
		return collate;
	}

	public void setCollate(boolean collate) {
		this.collate = collate;
	}
}
