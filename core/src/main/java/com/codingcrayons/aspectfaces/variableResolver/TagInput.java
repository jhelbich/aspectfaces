/*
 * Copyright (C) 2011-2014 CodingCrayons s.r.o.
 * All rights reserved.
 * Contact: CodingCrayons s.r.o. (info@codingcrayons.com)
 *
 * This file is part of AspectFaces.
 *
 * AspectFaces is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Foobar is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Foobar.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.codingcrayons.aspectfaces.variableResolver;

class TagInput {
	// this is to parse
	private final StringBuilder input;
	private final int length;
	// current index
	private int index = 0;

	TagInput(String input) {
		this.input = new StringBuilder(input);
		this.length = input.length();
	}

	TagInput(StringBuilder input) {
		if (input != null) {
			this.input = input;
			this.length = input.length();
		} else {
			this.input = new StringBuilder("");
			this.length = 0;
		}
	}

	char readChar() throws TagParserException {
		if (hasNext()) {
			return input.charAt(index++);
		} else {
			throw new TagParserException(this, "Unexpected end of file");
		}
	}

	boolean hasNext() {
		return index < length;
	}

	StringBuilder getOriginalInput() {
		return input;
	}

	int getCurrentIndex() {
		return index;
	}
}
