/*
 * Copyright (C) 2011-2014 CodingCrayons s.r.o.
 * All rights reserved.
 * Contact: CodingCrayons s.r.o. (info@codingcrayons.com)
 *
 * This file is part of AspectFaces.
 *
 * AspectFaces is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Foobar is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Foobar.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.codingcrayons.aspectfaces.variableResolver;

import com.codingcrayons.aspectfaces.exceptions.AFException;

public class TagParserException extends AFException {

	private static final long serialVersionUID = 6201914441641226768L;

	private final String tag;
	private String tagName = null;

	private final int position;

	private final String context;

	public TagParserException(String tag, int position, String context) {
		super("Parsing problem");
		this.tag = tag;
		this.position = position;
		this.context = context;
	}

	public TagParserException(TagInput tagInput, String context) {
		super("Parsing problem");
		this.tag = tagInput.getOriginalInput().toString();
		this.position = tagInput.getCurrentIndex();
		this.context = context;
	}

	public String getTag() {
		return tag;
	}

	public int getPosition() {
		return position;
	}

	public String getContext() {
		return context;
	}

	@Override
	public String getMessage() {
		if (tagName != null) {
			return "[tagName=" + tagName + "] Parsing tag at position:" + position + ". " + context;
		} else {
			return "Parsing tag at position:" + position + ". " + context;
		}
	}

	public String getTagName() {
		return tagName;
	}

	public void setTagName(String tagName) {
		this.tagName = tagName;
	}
}
