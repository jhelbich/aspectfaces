/*
 * Copyright (C) 2011-2014 CodingCrayons s.r.o.
 * All rights reserved.
 * Contact: CodingCrayons s.r.o. (info@codingcrayons.com)
 *
 * This file is part of AspectFaces.
 *
 * AspectFaces is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Foobar is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Foobar.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.codingcrayons.aspectfaces.variableResolver;

import java.util.List;

import javax.el.ValueExpression;

import com.codingcrayons.aspectfaces.annotation.registration.pointCut.properties.Variable;
import com.codingcrayons.aspectfaces.el.AFContext;
import com.codingcrayons.aspectfaces.el.ELUtil;
import com.codingcrayons.aspectfaces.variableResolver.LexicalAnalyser.LexicalSymbol;

public class TagVariableResolver {

	public TagVariableResolver() {
	}

	public StringBuilder resolve(String tagName, String input, String startBoundary, String endBoundary,
								 List<Variable> localVars) throws TagParserException {
		return resolve(tagName, new StringBuilder(input), startBoundary, endBoundary, localVars);
	}

	public StringBuilder resolve(String tagName, StringBuilder input, String startBoundary, String endBoundary,
								 List<Variable> localVars) throws TagParserException {

		// fill in all variables
		AFContext variableContext = ELUtil.makeExtendedVariableContext(localVars);
		LexicalAnalyser la = new LexicalAnalyser(input, startBoundary, endBoundary);
		StringBuilder out = new StringBuilder();
		try {
			do {
				la.readSymbol();

				if (la.getLexicalSymbol().isEvaluable()) {
					StringBuilder toEval = la.getLexicalSymbolContent();

					try {
						ValueExpression ve = ELUtil.createValueExpression(variableContext, "#{" + toEval.toString()
							+ "}", String.class);
						String evaluated = (String) ve.getValue(variableContext);
						out.append(evaluated);
					} catch (Exception e) {
					}
				} else if (!la.getLexicalSymbol().isEnd()) {
					out.append(la.getLexicalSymbolContent());
				}
			} while (la.getLexicalSymbol() != LexicalSymbol.END);
		} catch (TagParserException e) {
			e.setTagName(tagName);
			throw e;
		}
		return out;
	}
}
