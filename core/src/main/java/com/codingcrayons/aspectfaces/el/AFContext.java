/*
 * Copyright (C) 2011-2014 CodingCrayons s.r.o.
 * All rights reserved.
 * Contact: CodingCrayons s.r.o. (info@codingcrayons.com)
 *
 * This file is part of AspectFaces.
 *
 * AspectFaces is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Foobar is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Foobar.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.codingcrayons.aspectfaces.el;

import javax.el.ArrayELResolver;
import javax.el.BeanELResolver;
import javax.el.CompositeELResolver;
import javax.el.ELContext;
import javax.el.ELResolver;
import javax.el.FunctionMapper;
import javax.el.ListELResolver;
import javax.el.MapELResolver;
import javax.el.VariableMapper;

public class AFContext extends ELContext {

	private final CompositeELResolver resolver;
	private final AFResolver afResolver;

	public AFContext() {
		afResolver = new AFResolver();
		CompositeELResolver compositeResolver = new AFCompositeELResolver();
		compositeResolver.add(afResolver);
		compositeResolver.add(new MapELResolver(true));
		compositeResolver.add(new ListELResolver(true));
		compositeResolver.add(new ArrayELResolver(true));
		compositeResolver.add(new BeanELResolver(true));
		this.resolver = compositeResolver;
	}

	@Override
	public ELResolver getELResolver() {
		return resolver;
	}

	@Override
	public FunctionMapper getFunctionMapper() {
		return null;
	}

	@Override
	public VariableMapper getVariableMapper() {
		return null;
	}

	/**
	 * Adds new variable to EL context.
	 *
	 * @param name  - variable name
	 * @param value - variable value
	 * @param type  - variable type
	 */
	public void setVariable(String name, Object value, Class<?> type) {
		afResolver.addVariable(name, value, type);
	}
}
