/*
 * Copyright (C) 2011-2014 CodingCrayons s.r.o.
 * All rights reserved.
 * Contact: CodingCrayons s.r.o. (info@codingcrayons.com)
 *
 * This file is part of AspectFaces.
 *
 * AspectFaces is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Foobar is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Foobar.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.codingcrayons.aspectfaces.el;

import java.beans.FeatureDescriptor;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.TreeMap;

import javax.el.ELContext;
import javax.el.ELResolver;
import javax.el.PropertyNotWritableException;

public class AFResolver extends ELResolver {

	private static class Variable {

		private String name;
		private Object value;
		private Class<?> type;

		public Variable(String name, Object value, Class<?> type) {
			this.name = name;
			this.value = value;
			this.type = type;
		}

		private String getName() {
			return name;
		}

		Object getValue() {
			return value;
		}

		Class<?> getType() {
			return type;
		}
	}

	private final TreeMap<String, Variable> variableMap = new TreeMap<String, Variable>();

	/**
	 * Adds new variable.
	 *
	 * @param name  - variable name
	 * @param value - variable value
	 * @param type  - variable type
	 */
	public void addVariable(String name, Object value, Class<?> type) {
		variableMap.put(name, new Variable(name, value, type));
	}

	@Override
	public Object getValue(ELContext context, Object base, Object property) {
		if (base != null) {
			return null;
		}
		Variable variable = variableMap.get(property.toString());
		if (variable != null) {
			context.setPropertyResolved(true);
			return variable.getValue();
		}
		return null;
	}

	@Override
	public Class<?> getType(ELContext context, Object base, Object property) {
		// for read only variables not necessary to implement
		if (base != null) {
			return null;
		}
		Variable variable = variableMap.get(property.toString());
		if (variable != null) {
			context.setPropertyResolved(true);
			return variable.getType();
		}
		return null;
	}

	@Override
	public void setValue(ELContext context, Object base, Object property, Object value) {
		throw new PropertyNotWritableException();
	}

	@Override
	public boolean isReadOnly(ELContext context, Object base, Object property) {
		return true;
	}

	@Override
	public Iterator<FeatureDescriptor> getFeatureDescriptors(ELContext context, Object base) {
		if (base != null) {
			return null;
		}
		List<FeatureDescriptor> list = new ArrayList<FeatureDescriptor>(variableMap.size());
		for (Variable variable : variableMap.values()) {
			FeatureDescriptor fd = new FeatureDescriptor();
			fd.setName(variable.getName());
			fd.setDisplayName(variable.getName());
			fd.setPreferred(true);
			list.add(fd);
		}
		return list.iterator();
	}

	@Override
	public Class<?> getCommonPropertyType(ELContext context, Object base) {
		if (base != null) {
			return null;
		}
		return String.class;
	}
}
