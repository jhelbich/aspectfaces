/*
 * Copyright (C) 2011-2014 CodingCrayons s.r.o.
 * All rights reserved.
 * Contact: CodingCrayons s.r.o. (info@codingcrayons.com)
 *
 * This file is part of AspectFaces.
 *
 * AspectFaces is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Foobar is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Foobar.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.codingcrayons.aspectfaces.configuration.holders;

import java.io.File;

import com.codingcrayons.aspectfaces.configuration.Configuration;
import com.codingcrayons.aspectfaces.configuration.StaticConfiguration;
import com.codingcrayons.aspectfaces.exceptions.AFException;

import org.testng.annotations.BeforeMethod;

import static org.testng.Assert.assertEquals;
import static org.testng.Assert.fail;

public class FileConfigurationHolderTest {

	private Configuration configration;
	private ConfigurationHolder holder;
	private File file;

	@BeforeMethod
	public void setUp() {
		try {
			file = new File("test/com/aspectfaces/config.xml");
			configration = new StaticConfiguration(file.getName());
			holder = new FileConfigurationHolder(configration, file, true, true);
		} catch (Exception e) {
			fail("An unexpected exception: " + e.getMessage());
		}
	}

	public void testGetConfiguration() {
		try {
			assertEquals(holder.getConfiguration().getName(), configration.getName());
		} catch (AFException e) {
			fail("An unexpected exception: " + e.getMessage());
		}
	}
}
