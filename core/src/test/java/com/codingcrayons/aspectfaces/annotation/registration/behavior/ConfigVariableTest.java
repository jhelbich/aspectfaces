/*
 * Copyright (C) 2011-2014 CodingCrayons s.r.o.
 * All rights reserved.
 * Contact: CodingCrayons s.r.o. (info@codingcrayons.com)
 *
 * This file is part of AspectFaces.
 *
 * AspectFaces is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Foobar is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Foobar.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.codingcrayons.aspectfaces.annotation.registration.behavior;

import com.codingcrayons.aspectfaces.annotation.registration.pointCut.properties.Variable;

import org.testng.annotations.Test;

import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertFalse;
import static org.testng.Assert.assertTrue;

public class ConfigVariableTest {

	private Variable var;

	@Test
	public void testCreateDefault() {
		String name = "name";
		String value = "value";
		var = new Variable(name, value);
		assertEquals(var.getName(), name);
		assertEquals(var.getValue(), value);
		assertTrue(var.isRedefinable());
	}

	@Test
	public void testCreate() {
		String name = "name";
		String value = "value";
		boolean redefinable = false;
		var = new Variable(name, value, redefinable);
		assertEquals(var.getName(), name);
		assertEquals(var.getValue(), value);
		assertFalse(var.isRedefinable());
	}

	@Test
	public void testEquals() {
		String name = "name";
		String value = "value";
		var = new Variable(name, value);
		assertEquals(new Variable(name, value), var);
	}

	@Test
	public void testEqualDifferentRedefinable() {
		String name = "name";
		String value = "value";
		var = new Variable(name, value, true);
		assertFalse(var.equals(new Variable(name, value, false)));
	}

	@Test
	public void testEqualDifferentName() {
		String value = "value";
		var = new Variable("name1", value);
		assertFalse(var.equals(new Variable("name2", value)));
	}

	@Test
	public void testEqualDifferentValue() {
		String name = "name";
		var = new Variable(name, "value1");
		assertFalse(var.equals(new Variable(name, "value2")));
	}

	@Test
	public void testEqualsNull() {
		String name = "name";
		String value = "value";
		var = new Variable(name, value);
		assertFalse(var.equals(null));
	}

	@Test
	public void testEqualsObject() {
		String name = "name";
		String value = "value";
		var = new Variable(name, value);
		assertFalse(var.equals(new Object()));
	}

	@Test
	public void testEqualsNameNull() {
		String value = "value";
		var = new Variable(null, value);
		assertFalse(var.equals(new Variable("name", value)));
	}

	@Test
	public void testEqualsValueNull() {
		String name = "name";
		var = new Variable(name, null);
		assertFalse(var.equals(new Variable(name, "value")));
	}

	@Test
	public void testEqualsOtherNameNull() {
		String value = "value";
		var = new Variable("name", value);
		assertFalse(var.equals(new Variable(null, value)));
	}

	@Test
	public void testEqualsOtherValueNull() {
		String name = "name";
		var = new Variable(name, "value");
		assertFalse(var.equals(new Variable(name, null)));
	}

	@Test
	public void testEqualsBothNameNull() {
		var = new Variable(null, "value");
		assertTrue(var.equals(new Variable(null, "value")));
	}

	@Test
	public void testEqualsBothValueNull() {
		var = new Variable("name", null);
		assertTrue(var.equals(new Variable("name", null)));
	}
}
