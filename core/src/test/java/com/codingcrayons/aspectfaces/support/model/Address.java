/*
 * Copyright (C) 2011-2014 CodingCrayons s.r.o.
 * All rights reserved.
 * Contact: CodingCrayons s.r.o. (info@codingcrayons.com)
 *
 * This file is part of AspectFaces.
 *
 * AspectFaces is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Foobar is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Foobar.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.codingcrayons.aspectfaces.support.model;

import com.codingcrayons.aspectfaces.annotations.UiCollection;
import com.codingcrayons.aspectfaces.annotations.UiFormOrder;
import com.codingcrayons.aspectfaces.annotations.UiFormProfiles;
import com.codingcrayons.aspectfaces.annotations.UiSize;
import com.codingcrayons.aspectfaces.annotations.UiTableOrder;
import com.codingcrayons.aspectfaces.annotations.UiTableProfiles;

public class Address implements java.io.Serializable {

	private static final long serialVersionUID = 1L;
	private Long id;
	private Owner owner;
	private String country;
	private String city;
	private String street;
	private Integer postcode;
	private Long version;

	public Address() {
	}

	public Address(Long id, Owner owner, String country, String city, String street, Integer postcode) {
		this.id = id;
		this.owner = owner;
		this.country = country;
		this.city = city;
		this.street = street;
		this.postcode = postcode;
	}

	@UiTableOrder(1)
	@UiFormOrder(1)
	@UiFormProfiles({"edit"})
	public Long getId() {
		return this.id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	@UiCollection(name = "ownerList.resultList", itemRepresentation = "#{item.lastname}, #{item.firstname}")
	@UiTableOrder(7)
	@UiFormOrder(7)
	@UiTableProfiles({"full-list"})
	@UiFormProfiles({"edit", "new"})
	public Owner getOwner() {
		return this.owner;
	}

	public void setOwner(Owner owner) {
		this.owner = owner;
	}

	@UiTableOrder(3)
	@UiFormOrder(3)
	public String getCountry() {
		return this.country;
	}

	public void setCountry(String country) {
		this.country = country;
	}

	@UiTableOrder(4)
	@UiFormOrder(4)
	public String getCity() {
		return this.city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	@UiSize(30)
	@UiTableOrder(5)
	@UiFormOrder(5)
	public String getStreet() {
		return this.street;
	}

	public void setStreet(String street) {
		this.street = street;
	}

	@UiTableOrder(6)
	@UiFormOrder(6)
	public Integer getPostcode() {
		return this.postcode;
	}

	public void setPostcode(Integer postcode) {
		this.postcode = postcode;
	}

	public Long getVersion() {
		return this.version;
	}

	public void setVersion(Long version) {
		this.version = version;
	}
}
