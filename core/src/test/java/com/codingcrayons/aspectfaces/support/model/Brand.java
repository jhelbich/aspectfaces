/*
 * Copyright (C) 2011-2014 CodingCrayons s.r.o.
 * All rights reserved.
 * Contact: CodingCrayons s.r.o. (info@codingcrayons.com)
 *
 * This file is part of AspectFaces.
 *
 * AspectFaces is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Foobar is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Foobar.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.codingcrayons.aspectfaces.support.model;

import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import com.codingcrayons.aspectfaces.annotations.UiFormOrder;
import com.codingcrayons.aspectfaces.annotations.UiFormProfiles;
import com.codingcrayons.aspectfaces.annotations.UiTableOrder;
import com.codingcrayons.aspectfaces.annotations.UiText;

public class Brand implements java.io.Serializable {

	private static final long serialVersionUID = 1L;
	private Long id;
	private String name;
	private Date founded;
	private String characterization;
	private Set<Car> cars = new HashSet<Car>(0);
	private Long version;

	public Brand() {
	}

	public Brand(Long id, String name) {
		this.id = id;
		this.name = name;
	}

	public Brand(Long id, String name, Date founded, String characterization, Set<Car> cars) {
		this.id = id;
		this.name = name;
		this.founded = founded;
		this.characterization = characterization;
		this.cars = cars;
	}

	@UiTableOrder(1)
	@UiFormOrder(1)
	@UiFormProfiles({"edit"})
	public Long getId() {
		return this.id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	@UiFormProfiles({"edit", "new"})
	public Date getFounded() {
		return this.founded;
	}

	public void setFounded(Date founded) {
		this.founded = founded;
	}

	@UiText(cols = 40, rows = 4)
	public String getCharacterization() {
		return this.characterization;
	}

	public void setCharacterization(String characterization) {
		this.characterization = characterization;
	}

	public Set<Car> getCars() {
		return this.cars;
	}

	public void setCars(Set<Car> cars) {
		this.cars = cars;
	}

	public Long getVersion() {
		return this.version != null ? this.version : 0;
	}

	public void setVersion(Long version) {
		this.version = version;
	}
}
