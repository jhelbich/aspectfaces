/*
 * Copyright (C) 2011-2014 CodingCrayons s.r.o.
 * All rights reserved.
 * Contact: CodingCrayons s.r.o. (info@codingcrayons.com)
 *
 * This file is part of AspectFaces.
 *
 * AspectFaces is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Foobar is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Foobar.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.codingcrayons.aspectfaces.support.model;

import java.math.BigDecimal;
import java.util.Date;

import com.codingcrayons.aspectfaces.annotations.UiCollection;
import com.codingcrayons.aspectfaces.annotations.UiFormOrder;
import com.codingcrayons.aspectfaces.annotations.UiFormProfiles;
import com.codingcrayons.aspectfaces.annotations.UiTableOrder;
import com.codingcrayons.aspectfaces.annotations.UiTableProfiles;
import com.codingcrayons.aspectfaces.annotations.UiText;

public class Car implements java.io.Serializable {

	private static final long serialVersionUID = 1L;
	private Long id;
	private Owner owner;
	private InsuranceCompany insuranceCompany;
	private String name;
	private CarType type;
	private Short year;
	private String description;
	private String vin;
	private String color;
	private Date technicalValidity;
	private Short maxpeople;
	private Short doors;
	private Brand brand;
	private String licencePlate;
	private BigDecimal price;
	private Long version;

	public Car() {
	}

	public Car(Long id, Owner owner, InsuranceCompany insuranceCompany, String name, CarType type, Short year,
			   String vin, String licencePlate) {
		this.id = id;
		this.owner = owner;
		this.insuranceCompany = insuranceCompany;
		this.name = name;
		this.type = type;
		this.year = year;
		this.vin = vin;
		this.licencePlate = licencePlate;
	}

	public Car(Long id, Owner owner, InsuranceCompany insuranceCompany, String name, CarType type, Short year,
			   String description, String vin, String color, Date technicalValidity, Short maxpeople, Short doors,
			   String licencePlate, BigDecimal price, Brand brand) {
		this.id = id;
		this.owner = owner;
		this.insuranceCompany = insuranceCompany;
		this.name = name;
		this.type = type;
		this.year = year;
		this.description = description;
		this.vin = vin;
		this.color = color;
		this.technicalValidity = technicalValidity;
		this.maxpeople = maxpeople;
		this.doors = doors;
		this.licencePlate = licencePlate;
		this.price = price;
		this.brand = brand;
	}

	@UiTableOrder(1)
	@UiFormOrder(1)
	@UiFormProfiles({"edit"})
	public Long getId() {
		return this.id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	@UiCollection(name = "ownerList.resultList", itemRepresentation = "#{item.lastname}, #{item.firstname}")
	@UiTableProfiles({"full-list"})
	@UiFormProfiles({"edit", "new"})
	public Owner getOwner() {
		return this.owner;
	}

	public void setOwner(Owner owner) {
		this.owner = owner;
	}

	@UiCollection(name = "insuranceCompanyList.resultList", itemRepresentation = "#{item.name}")
	@UiTableProfiles({"full-list"})
	@UiFormProfiles({"edit", "new"})
	public InsuranceCompany getInsuranceCompany() {
		return this.insuranceCompany;
	}

	public void setInsuranceCompany(InsuranceCompany insuranceCompany) {
		this.insuranceCompany = insuranceCompany;
	}

	@UiTableOrder(2)
	@UiFormOrder(2)
	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	@UiCollection(name = "listAction.carTypes")
	@UiTableOrder(3)
	@UiFormOrder(3)
	public CarType getType() {
		return this.type;
	}

	public void setType(CarType type) {
		this.type = type;
	}

	@UiTableOrder(5)
	@UiFormOrder(5)
	@UiFormProfiles({"edit", "new"})
	public Short getYear() {
		return this.year;
	}

	public void setYear(Short year) {
		this.year = year;
	}

	@UiTableOrder(10)
	@UiFormOrder(10)
	@UiText(cols = 40, rows = 4)
	public String getDescription() {
		return this.description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	@UiTableOrder(4)
	@UiFormOrder(1.1)
	public String getVin() {
		return this.vin;
	}

	public void setVin(String vin) {
		this.vin = vin;
	}

	@UiTableOrder(6)
	@UiFormOrder(2.2)
	public String getColor() {
		return this.color;
	}

	public void setColor(String color) {
		this.color = color;
	}

	@UiTableOrder(9)
	@UiFormOrder(9)
	@UiFormProfiles({"edit", "new"})
	public Date getTechnicalValidity() {
		return this.technicalValidity;
	}

	public void setTechnicalValidity(Date technicalValidity) {
		this.technicalValidity = technicalValidity;
	}

	@UiTableOrder(8)
	@UiFormOrder(8)
	@UiFormProfiles({"edit", "new"})
	public Short getMaxpeople() {
		return this.maxpeople;
	}

	public void setMaxpeople(Short maxpeople) {
		this.maxpeople = maxpeople;
	}

	@UiTableOrder(7)
	@UiFormOrder(7)
	@UiFormProfiles({"edit", "new"})
	public Short getDoors() {
		return this.doors;
	}

	public void setDoors(Short doors) {
		this.doors = doors;
	}

	@UiTableOrder(11)
	@UiFormOrder(11)
	public String getLicencePlate() {
		return this.licencePlate;
	}

	public void setLicencePlate(String licencePlate) {
		this.licencePlate = licencePlate;
	}

	@UiTableOrder(12)
	@UiFormOrder(12)
	@UiFormProfiles({"edit", "new"})
	public BigDecimal getPrice() {
		return this.price;
	}

	public void setPrice(BigDecimal price) {
		this.price = price;
	}

	@UiCollection(name = "brandList.resultList", itemRepresentation = "#{item.name}")
	@UiTableProfiles({"full-list"})
	@UiFormProfiles({"edit", "new"})
	public Brand getBrand() {
		return this.brand;
	}

	public void setBrand(Brand brand) {
		this.brand = brand;
	}

	public Long getVersion() {
		return this.version;
	}

	public void setVersion(Long version) {
		this.version = version;
	}
}
