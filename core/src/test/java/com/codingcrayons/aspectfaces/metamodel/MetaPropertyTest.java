/*
 * Copyright (C) 2011-2014 CodingCrayons s.r.o.
 * All rights reserved.
 * Contact: CodingCrayons s.r.o. (info@codingcrayons.com)
 *
 * This file is part of AspectFaces.
 *
 * AspectFaces is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Foobar is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Foobar.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.codingcrayons.aspectfaces.metamodel;

import java.util.ArrayList;
import java.util.List;

import com.codingcrayons.aspectfaces.annotation.registration.pointCut.properties.Variable;
import com.codingcrayons.aspectfaces.support.model.Address;
import com.codingcrayons.aspectfaces.support.model.Administrator;

import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertFalse;
import static org.testng.Assert.assertNull;
import static org.testng.Assert.assertTrue;

public class MetaPropertyTest {

	private MetaEntity entity;
	private MetaProperty property;

	@BeforeMethod
	public void setUp() {
		entity = new MetaEntity(Administrator.class.getName(), Administrator.class.getSimpleName());
		property = new MetaProperty(entity);
	}

	@Test
	public void testGetEntityDefault() {
		assertEquals(property.getMetaEntity(), entity);
	}

	@Test
	public void testGetEntity() {
		MetaEntity newEntity = new MetaEntity(Address.class.getName(), Address.class.getSimpleName());
		property.setMetaEntity(newEntity);
		assertEquals(property.getMetaEntity(), newEntity);
	}

	@Test
	public void testGetName() {
		String name = "id";
		property.setName(name);
		assertEquals(property.getName(), name);
	}

	@Test
	public void testGetNameDefault() {
		assertNull(property.getName());
	}

	@Test
	public void testGetVariableName() {
		property.setName("id");
		assertEquals(property.getVariableName(), "com.codingcrayons.aspectfaces.support.model.Administrator.id");
	}

	@Test
	public void testGetVariableNameDefault() {
		assertEquals(property.getVariableName(), "com.codingcrayons.aspectfaces.support.model.Administrator.null");
	}

	@Test
	public void testGetReturnType() {
		String type = "Long";
		property.setReturnType(type);
		assertEquals(property.getReturnType(), type);
	}

	@Test
	public void testGetReturnTypeDefault() {
		assertNull(property.getReturnType());
	}

	@Test
	public void testGetOrder() {
		Double order = new Double(1);
		property.setOrder(order);
		assertEquals(property.getOrder(), order);
	}

	@Test
	public void testGetOrderDefault() {
		assertNull(property.getOrder());
	}

	@Test
	public void getSpecifiedTag() {
		String tag = "tag";
		property.setSpecificTag(tag);
		assertEquals(property.getSpecificTag(), tag);
	}

	@Test
	public void getSpecifiedTagDefault() {
		assertNull(property.getSpecificTag());
	}

	@Test
	public void testIsApplicableDefault() {
		assertFalse(property.isApplicable());
	}

	@Test
	public void testIsApplicable() {
		boolean applicable = true;
		property.setApplicable(applicable);
		assertTrue(property.isApplicable());
	}

	@Test
	public void testGetEvaluableStringsDefault() {
		assertTrue(property.getEvaluableStrings().isEmpty());
	}

	@Test
	public void testGetEvaluableStrings() {
		List<String> evalStrings = new ArrayList<String>(3);
		evalStrings.add("true");
		property.addEvaluableString(evalStrings.get(0));
		evalStrings.add("false");
		property.addEvaluableString(evalStrings.get(1));
		evalStrings.add("1 == 1");
		property.addEvaluableString(evalStrings.get(2));
		assertEquals(property.getEvaluableStrings(), evalStrings);
	}

	@Test
	public void testGetTemplateVariables() {
		List<Variable> expected = new ArrayList<Variable>(3);
		expected.add(new Variable("var1", "var1", false, Variable.VarType.TEMPLATE));
		property.addVariable(expected.get(0));
		expected.add(new Variable("var2", "var2-unredefinable", false, Variable.VarType.TEMPLATE));
		property.addVariable(expected.get(1));
		expected.add(new Variable("var3", "var3-overridden", false, Variable.VarType.TEMPLATE));
		property.addVariable(new Variable("var3", "var3", true, Variable.VarType.TEMPLATE));
		property.addVariable(expected.get(2));
		property.addVariable(new Variable("var2", "var2", false, Variable.VarType.TEMPLATE));

		List<Variable> configVars = new ArrayList<Variable>(3);
		configVars.add(new Variable("var4", "var4", false, Variable.VarType.CONFIG));
		configVars.add(new Variable("var5", "var5", false, Variable.VarType.CONFIG));
		configVars.add(new Variable("var6", "var6", false, Variable.VarType.CONFIG));
		assertEquals(property.getTemplateVariables(), expected);
	}

	@Test
	public void testGetConfigVariables() {
		List<Variable> expected = new ArrayList<Variable>(3);
		expected.add(new Variable("var1", "var1", false, Variable.VarType.CONFIG));
		property.addVariable(expected.get(0));
		expected.add(new Variable("var2", "var2-unredefinable", false, Variable.VarType.CONFIG));
		property.addVariable(expected.get(1));
		expected.add(new Variable("var3", "var3-overridden", false, Variable.VarType.CONFIG));
		property.addVariable(new Variable("var3", "var3", true, Variable.VarType.CONFIG));
		property.addVariable(expected.get(2));
		property.addVariable(new Variable("var2", "var2", false, Variable.VarType.CONFIG));

		List<Variable> templateVars = new ArrayList<Variable>(3);
		templateVars.add(new Variable("var4", "var4", false, Variable.VarType.TEMPLATE));
		templateVars.add(new Variable("var5", "var5", false, Variable.VarType.TEMPLATE));
		templateVars.add(new Variable("var6", "var6", false, Variable.VarType.TEMPLATE));
		assertEquals(property.getConfigVariables(), expected);
	}

	@Test
	public void testAddVariables() {
		property.addVariable(new Variable("var3", "var3", true, Variable.VarType.CONFIG));
		List<Variable> configVars = new ArrayList<Variable>(3);
		configVars.add(new Variable("var3", "var3-overridden", false, Variable.VarType.CONFIG));
		configVars.add(new Variable("var1", "var1", false, Variable.VarType.CONFIG));
		configVars.add(new Variable("var2", "var2-unredefinable", false, Variable.VarType.CONFIG));
		property.addVariables(configVars);
		property.addVariable(new Variable("var2", "var2", false, Variable.VarType.CONFIG));

		property.addVariable(new Variable("var4", "var4", true, Variable.VarType.TEMPLATE));
		List<Variable> templateVars = new ArrayList<Variable>(3);
		templateVars.add(new Variable("var4", "var4-overridden", false, Variable.VarType.TEMPLATE));
		templateVars.add(new Variable("var6", "var6", false, Variable.VarType.TEMPLATE));
		templateVars.add(new Variable("var5", "var5-unredefinable", false, Variable.VarType.TEMPLATE));
		property.addVariables(templateVars);
		property.addVariable(new Variable("var5", "var5", false, Variable.VarType.TEMPLATE));

		assertEquals(property.getConfigVariables(), configVars);
		assertEquals(property.getTemplateVariables(), templateVars);
	}

	@Test
	public void testConstructor() {
		String type = "Long";
		String name = "id";
		Double order = new Double(1);
		String tag = "tag";
		property = new MetaProperty(entity, type, name, order, tag);
		assertEquals(property.getMetaEntity(), entity);
		assertEquals(property.getReturnType(), type);
		assertEquals(property.getName(), name);
		assertEquals(property.getVariableName(), "com.codingcrayons.aspectfaces.support.model.Administrator.id");
		assertEquals(property.getOrder(), order);
		assertEquals(property.getSpecificTag(), tag);
	}
}
