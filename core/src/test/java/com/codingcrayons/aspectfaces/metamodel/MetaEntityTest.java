/*
 * Copyright (C) 2011-2014 CodingCrayons s.r.o.
 * All rights reserved.
 * Contact: CodingCrayons s.r.o. (info@codingcrayons.com)
 *
 * This file is part of AspectFaces.
 *
 * AspectFaces is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Foobar is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Foobar.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.codingcrayons.aspectfaces.metamodel;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.PrintStream;
import java.util.List;

import com.codingcrayons.aspectfaces.AFWeaver;
import com.codingcrayons.aspectfaces.TestCase;
import com.codingcrayons.aspectfaces.composition.UIFragmentComposer;
import com.codingcrayons.aspectfaces.configuration.Configuration;
import com.codingcrayons.aspectfaces.configuration.Context;
import com.codingcrayons.aspectfaces.configuration.Mapping;
import com.codingcrayons.aspectfaces.configuration.Settings;
import com.codingcrayons.aspectfaces.exceptions.AnnotationDescriptorNotFoundException;
import com.codingcrayons.aspectfaces.exceptions.AnnotationNotFoundException;
import com.codingcrayons.aspectfaces.exceptions.AnnotationNotRegisteredException;
import com.codingcrayons.aspectfaces.exceptions.ConfigurationNotSetException;
import com.codingcrayons.aspectfaces.exceptions.EvaluatorException;
import com.codingcrayons.aspectfaces.exceptions.TemplateFileAccessException;
import com.codingcrayons.aspectfaces.exceptions.TemplateFileNotFoundException;
import com.codingcrayons.aspectfaces.support.model.Address;
import com.codingcrayons.aspectfaces.support.model.AddressWithUICollision;
import com.codingcrayons.aspectfaces.support.model.Administrator;
import com.codingcrayons.aspectfaces.variableResolver.TagParserException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertTrue;

public class MetaEntityTest extends TestCase {

	private MetaEntity metaEntity;

	@BeforeMethod
	public void setUp() {
		this.metaEntity = new MetaEntity(Administrator.class.getName(), Administrator.class.getSimpleName());
	}

	@Test
	public void testGetName() {
		assertEquals(this.metaEntity.getName(), Administrator.class.getName());
	}

	@Test
	public void testGetSimpleName() {
		assertEquals(this.metaEntity.getSimpleName(), Administrator.class.getSimpleName());
	}

	@Test
	public void testSetName() {
		this.metaEntity.setName(Address.class.getName());
		assertEquals(metaEntity.getName(), Address.class.getName());
	}

	@Test
	public void testSetSimpleName() {
		this.metaEntity.setSimpleName(Address.class.getSimpleName());
		assertEquals(metaEntity.getSimpleName(), Address.class.getSimpleName());
	}

	@Test
	public void testOrder() throws AnnotationDescriptorNotFoundException, EvaluatorException,
		AnnotationNotRegisteredException, AnnotationNotFoundException, TemplateFileNotFoundException,
		TemplateFileAccessException, ConfigurationNotSetException, TagParserException {

		Context context = new Context();
		JavaInspector inspector = initBasicAF(context, Address.class);

		String out = null;

		context.setOrderAnnotation("com.codingcrayons.aspectfaces.annotations.UiFormOrder");
		out = getAFGeneratedOutput(context, inspector);
		assertEquals(out, "id\ncountry\ncity\nstreet\npostcode\nowner\nversion\n");

		context.setOrderAnnotation("com.codingcrayons.aspectfaces.annotations.NONE");
		out = getAFGeneratedOutput(context, inspector);
		assertEquals(out, "city\ncountry\nid\nowner\npostcode\nstreet\nversion\n");
	}

	@Test
	public void testOrderCollision() throws AnnotationDescriptorNotFoundException, EvaluatorException,
		AnnotationNotRegisteredException, AnnotationNotFoundException, TemplateFileNotFoundException,
		TemplateFileAccessException, ConfigurationNotSetException, TagParserException, SecurityException,
		NoSuchFieldException, IllegalArgumentException, IllegalAccessException {

		LoggerMock loggerMock = new LoggerMock(UIFragmentComposer.class);
		ByteArrayOutputStream outputStream = loggerMock.getOutputStream();

		Context context = new Context();

		JavaInspector inspector = initBasicAF(context, AddressWithUICollision.class);

		String out = null;

		context.setOrderAnnotation("com.codingcrayons.aspectfaces.annotations.UiFormOrder");
		out = getAFGeneratedOutput(context, inspector);
		assertEquals(out, "id\nowner\ncountry\ncity\nstreet\npostcode\nversion\n");
		assertTrue(outputStream.toString().contains("Colliding field order with value"));
		outputStream.reset();

		context.setOrderAnnotation("com.codingcrayons.aspectfaces.annotations.UiOrder");
		out = getAFGeneratedOutput(context, inspector);
		assertEquals(out, "id\nowner\ncity\ncountry\npostcode\nstreet\nversion\n");
		assertTrue(outputStream.toString().contains("Colliding field order with value"));

		// reset logger back
		loggerMock.redirectBack();
	}

	private String getAFGeneratedOutput(Context context, JavaInspector inspector) throws EvaluatorException,
		AnnotationNotRegisteredException, AnnotationNotFoundException, TemplateFileNotFoundException,
		TemplateFileAccessException, ConfigurationNotSetException, TagParserException {

		List<MetaProperty> metaProperties = inspector.inspect(context);
		UIFragmentComposer composer = new UIFragmentComposer();
		composer.addAllFields(metaProperties);
		return composer.composeForm(context).getCompleteForm().toString();
	}

	private static class LoggerMock {
		private Class<?> initClass;
		private Object oldStream;
		private String field = "TARGET_STREAM";
		private Logger logger;

		public LoggerMock(Class<?> initClass) {
			this.initClass = initClass;
		}

		public ByteArrayOutputStream getOutputStream() throws NoSuchFieldException, IllegalAccessException {
			logger = LoggerFactory.getLogger(initClass);
			// Create a stream to hold the output
			ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
			PrintStream ps = new PrintStream(outputStream);

			// NOTE redirect the logger so we can track the content!
			java.lang.reflect.Field fieldOfLogger = logger.getClass().getDeclaredField(field);
			fieldOfLogger.setAccessible(true);
			oldStream = fieldOfLogger.get(logger);
			fieldOfLogger.set(logger, ps);
			return outputStream;
		}

		public void redirectBack() throws IllegalArgumentException, IllegalAccessException, SecurityException,
			NoSuchFieldException {
			java.lang.reflect.Field fieldOfLogger = logger.getClass().getDeclaredField(field);
			fieldOfLogger.setAccessible(true);
			fieldOfLogger.set(logger, oldStream);
		}
	}

	private JavaInspector initBasicAF(Context context, Class<?> whatToInspect)
		throws AnnotationDescriptorNotFoundException {
		final String template = "$fieldName$\n";

		JavaInspector inspector = new JavaInspector(whatToInspect);
		AFWeaver.registerAllAnnotations();
		AFWeaver.setOpenVariableBoundaryIdentifier("$");
		AFWeaver.setCloseVariableBoundaryIdentifier("$");

		// set config (ignores fields)
		Configuration configuration = new Configuration("NAME") {
			@Override
			public String getAbsolutePath() {
				return "";
			}

			@Override
			protected StringBuilder getTemplate(String fileTemplatePath, String prefix)
				throws TemplateFileNotFoundException {
				return new StringBuilder(template);
			}

			@Override
			protected String getDelimiter() {
				return File.separator;
			}
		};
		configuration.addMapping(new Mapping("String", "path"));
		configuration.addMapping(new Mapping("Long", "path"));
		configuration.addMapping(new Mapping("Owner", "path"));
		configuration.addMapping(new Mapping("Integer", "path"));

		Settings settings = new Settings();
		configuration.setSettings(settings);
		context.setConfiguration(configuration);
		return inspector;
	}
}
