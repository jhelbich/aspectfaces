/*
 * Copyright (C) 2011-2014 CodingCrayons s.r.o.
 * All rights reserved.
 * Contact: CodingCrayons s.r.o. (info@codingcrayons.com)
 *
 * This file is part of AspectFaces.
 *
 * AspectFaces is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Foobar is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Foobar.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.codingcrayons.aspectfaces.configuration;

import java.net.MalformedURLException;
import java.net.URL;

import com.codingcrayons.aspectfaces.AFWeaver;
import com.codingcrayons.aspectfaces.exceptions.ConfigurationFileNotFoundException;
import com.codingcrayons.aspectfaces.exceptions.ConfigurationNotFoundException;
import com.codingcrayons.aspectfaces.exceptions.ConfigurationParsingException;
import com.codingcrayons.aspectfaces.plugins.j2ee.configuration.ServerConfiguration;

import org.testng.annotations.Test;

import static org.testng.Assert.assertNotNull;

public class ConfigurationStorageTest {

	@Test(expectedExceptions = ConfigurationFileNotFoundException.class)
	public void testConfigurationNotFoundAtUrl() throws MalformedURLException, ConfigurationNotFoundException,
		ConfigurationFileNotFoundException, ConfigurationParsingException {
		String name = "config";
		Configuration configuration = new ServerConfiguration(name, null);
		AFWeaver.addConfiguration(configuration, new URL("http://fail"), true, true);
		assertNotNull(ConfigurationStorage.getInstance().getConfiguration(name));
	}
}
